package ru.simplex_software.hubzilla4j;


import com.google.gson.TypeAdapter;
import feign.Feign;
import feign.Response;
import feign.auth.BasicAuthRequestInterceptor;
import feign.gson.GsonDecoder;
import feign.slf4j.Slf4jLogger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static final Logger LOG= LoggerFactory.getLogger(AppTest.class);

    static HubzillaClient client;

    @BeforeAll
    static void setup() {

        LOG.info("@BeforeAll - executes once before all test methods in this class");
        List<TypeAdapter> tal=new ArrayList();
//        tal.add(new TypeAdapter<Instant>(){
//            @Override
//            public void write(JsonWriter jsonWriter, Object o) throws IOException {
//
//            }
//
//            @Override
//            public Object read(JsonReader jsonReader) throws IOException {
//                return null;
//            }
//        });

        client = Feign.builder()
                .decoder(new GsonDecoder())
                .logLevel(feign.Logger.Level.FULL)
                .logger(new Slf4jLogger(HubzillaClient.class))
                .requestInterceptor(new BasicAuthRequestInterceptor("hubzilla4j","laiSii8a"))
                .target(HubzillaClient.class, "https://hubzilla.ru");
    }

    @BeforeEach
    void init() {
        LOG.info("@BeforeEach - executes before each test method in this class");

    }

    @Test
    public void testPost() throws Exception{
        ItemResult simplePost = client.createSimplePost("test", "TestContent In Body");
        LOG.info(""+simplePost.getItemId());
    }
    @Test
    public void testStream() throws Exception{
        client.myStream();
    }
    @Test
    public void testComment() throws Exception{
        ItemResult simplePost = client.createSimplePost("test", "TestContent In Body");
        LOG.info("body: {}", simplePost.getItem());
        ItemResult  myComment = client.createComment(simplePost.getItemId(), "", "my comment");
        LOG.info("body: {}", myComment.getItem().getBody());
    }
}

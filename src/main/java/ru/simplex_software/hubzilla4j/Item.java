package ru.simplex_software.hubzilla4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

/**
 * .
 */
public class Item {
    private static final Logger LOG = LoggerFactory.getLogger(Item.class);
    private Long id;
    private  String mid;
    private Long  aid;
    private Long uid;
    private Long parent;
    private String parent_mid;
    private String thr_parent;
    //private Instant created;
    //..
    private String title;
    private String body;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Long getAid() {
        return aid;
    }

    public void setAid(Long aid) {
        this.aid = aid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getParent_mid() {
        return parent_mid;
    }

    public void setParent_mid(String parent_mid) {
        this.parent_mid = parent_mid;
    }

    public String getThr_parent() {
        return thr_parent;
    }

    public void setThr_parent(String thr_parent) {
        this.thr_parent = thr_parent;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

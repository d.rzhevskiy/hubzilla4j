package ru.simplex_software.hubzilla4j;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;

/**
 * .
 */
public interface HubzillaClient {


    @Headers({"Content-Type: application/x-www-form-urlencoded", "Accept: application/json"})
    @RequestLine("POST /api/z/1.0/item/update")
    @Body("body={body}&title={title}&mimetype=text/markdown")
    public ItemResult createSimplePost(@Param("title")String title, @Param("body") String body );

    @Headers({"Content-Type: application/x-www-form-urlencoded", "Accept: application/json"})
    @RequestLine("POST /api/z/1.0/item/update")
    @Body("body={body}&title={title}&parent={parent}")
    public ItemResult  createComment(@Param("parent")String parent, @Param("title")String title, @Param("body") String body );

    @Headers({"Content-Type: application/x-www-form-urlencoded", "Accept: application/json"})
    @RequestLine("GET /api/z/1.0/channel/stream")
    public Response myStream();

    }

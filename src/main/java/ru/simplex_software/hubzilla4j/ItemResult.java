package ru.simplex_software.hubzilla4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.annotations.SerializedName;

/**
 * .
 */
public class ItemResult {
    private static final Logger LOG = LoggerFactory.getLogger(ItemResult.class);
    private boolean success;

    @SerializedName("item_id")
    private String itemId;
    @SerializedName("item")
    private Item item;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }


    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
